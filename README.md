# Maratona Behind the Code (IBM)

> Thiago Lopes <t4sl@protonmail.com>

- [Home](https://maratona.dev/pt)
- [IBM Cloud](https://cloud.ibm.com/)
  - Documentation
    - [Visual Recognition](https://cloud.ibm.com/docs/visual-recognition)
- [GitHub](https://github.com/maratonadev-br)
- [Discord](https://discord.gg/2NRPpcU)

## Desafio 1 - Cocamar

Links:

- [Desafio](https://youtu.be/Ur9ktogEkgo)
- [Tutorial](https://youtu.be/ODorFVi9bL4)
- [Deploy to IBM Cloud](https://cloud.ibm.com/devops/setup/deploy?repository=https://github.com/maratonadev-br/desafio-1-2020)

Resumo:

| Classe | Animal |
| --- | --- |
| lagarta | Anticarsia Gemmatalis |
| percevejo_marrom | Euschistus Heros |
| percevejo_verde | Nezara Viridula |
| percevejo_pequeno | Piezodorus Guildinii |

Os percevejos são da família _pentatomidae_, enquanto a lagarta é da família _lepidoptera_.
